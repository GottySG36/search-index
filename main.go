// Utility to extract specific sequences from a fastq file if it contains the desired index

package main

import (
    "bufio"
    "flag"
    "fmt"
    "compress/gzip"
    "log"
//    "io"
    "os"

    "bitbucket.org/GottySG36/sequtils"
)

var (
    defaultPrefix = "sample"
    defaultOutDir = "."

    synopsis = `Simple utility to read fastq files and extract sequences contain a specific index.
Multiple index can be searched in parallel for a single fastq file with at most 't' jobs.`
    uString  = `extract_seqs [-fsq <FILE1>] [-idx <JSON>] [-p <STR>] [-t INT] [-z]`

    fq   = flag.String( "fqs",  "-",    "Input fastq containing the sequences to look for.")
    idx  = flag.String( "idx",  "",     "index to look for.")
    b    = flag.Int(    "b",    0,    "Start position for sliding window.")
    e    = flag.Int(    "e",    -1,   "End position for sliding window.")
    m    = flag.Int(    "m",    1,   "The number of allowed mismatch.")
    pref = flag.String( "p",    "-",    "Prefix used to build output file names.")
    nseq = flag.Int(    "n",    1000000,      "Number of sequences to load at a time.")
    zip     = flag.Bool("z", false, "Specifies that the output result is to be gzip-compressed.")
)


var Usage = func() {
    fmt.Fprintf(flag.CommandLine.Output(), "%v\n\n", synopsis)
    fmt.Fprintf(flag.CommandLine.Output(), "Usage : %v\n\n", uString)

    fmt.Fprintf(flag.CommandLine.Output(), "Arguments of %s:\n", os.Args[0])
    flag.PrintDefaults()
}

func main() {
    flag.Usage = Usage
    flag.Parse()
    if *idx == "" {
        log.Fatalf("ERROR -:- Index required. Use -idx to specify one.")
    }
    if *e <= 0 {
        log.Fatalf("ERROR -:- You need to specify the end position of the sliding window using -e.")
    }

    var eof bool
    var err error
    var w *bufio.Writer
    var wg *gzip.Writer
    var of *os.File
    r, f, err := sequtils.OpenFastq(*fq)
    if err != nil {
        log.Fatalf("Error -:- OpenFastq : %v\n", err)
    }
    defer f.Close()

    outp := *pref
    if *pref != "-" {
        outp = fmt.Sprintf("%v.fastq", *pref)
        if *zip {
            outp = fmt.Sprintf("%v.gz", outp)
        }
    }
    if *zip {
        wg, of, err = sequtils.CreateFileGzip(outp)
        if err != nil {
            log.Fatalf("ERROR -:- CreateFileGzip : %v\n", err)
        }
    } else {
        w, of, err = sequtils.CreateFile(outp)
        if err != nil {
            log.Fatalf("ERROR -:- CreateFile : %v\n", err)
        }
    }
    defer of.Close()
    defer func(z bool) {
        if *zip {
            wg.Flush()
            wg.Close()
        } else {
            w.Flush()
        }
    }(*zip)

    for {
        if eof {
            break
        }
        fq, eof, err := sequtils.LoadNFastq(*nseq, r)
        if err != nil {
            log.Fatalf("ERROR -:- LoadNFastq : %v\n", err)
        }

        err = fq.SearchIndex(*idx, *b, *e, *m)
        if err != nil {
            log.Fatalf("%v\n", err)
        }

        if *zip {
            err = fq.WriteAppendGzip(wg)
            if err != nil {
                log.Fatalf("ERROR -:- WriteAppendGzip : %v\n", err)
            }
        } else {
            err = fq.WriteAppend(w)
            if err != nil {
                log.Fatalf("ERROR -:- WriteAppend : %v\n", err)
            }
        }
        if eof {
            break
        }
    }
}
